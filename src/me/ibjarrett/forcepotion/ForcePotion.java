package me.macguy8.bukkit.KeepPotions;

import java.util.Collection;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitScheduler;

public class ForcePotion extends JavaPlugin
  implements Listener
{
  private HashMap<String, Collection<PotionEffect>> effects = new HashMap();

  public void onEnable()
  {
    getServer().getPluginManager().registerEvents(this, this);
  }

  @EventHandler
  public void onPlayerDeath(PlayerDeathEvent event) {
    if (!event.getEntity().hasPermission("forcepotion.keep")) {
      return;
    }

    if (event.getEntity().getActivePotionEffects().isEmpty()) {
      return;
    }

    this.effects.put(event.getEntity().getName(), event.getEntity().getActivePotionEffects());
  }

  @EventHandler
  public void onPlayerRespawn(PlayerRespawnEvent event) {
    final Player p = event.getPlayer();

    Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
        @Override
      public void run() {
        if (ForcePotion.this.effects.containsKey(p.getName())) {
          for (PotionEffect e : (Collection)ForcePotion.this.effects.get(p.getName())) {
            p.addPotionEffect(e);
          }

          
          ForcePotion.this.effects.remove(p.getName());
        }
      }
    }
    , 20L);
  }
}